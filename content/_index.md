---
title: mycelium
subtitle: ''
share_img: "/media/mycelium.png"

---
Journal et notes d’étude du [#système alimentaire](/tags/syst%C3%A8me-alimentaire/), du monde de la [#restauration](/tags/restauration/), de l'[#agriculture](tags/agriculture/) et des [#techniques](/tags/technique/) et [#recherches](/tags/recherche/) qui en découlent.