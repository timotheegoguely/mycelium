---
title: Nourrir un million d’individus
subtitle: Comprendre les réalités d’un système défaillant
date: 2020-11-23T00:00:00+01:00
tags:
- système alimentaire
- culture alimentaire
- restauration
- éducation
disclaimer: La traduction de la conversation est personnelle et des passages ont été
  légèrement modifiés dans l’intérêt d'un récit cohérent.
share_img: ''

---
Dan Giusti, chef issu de la restauration gastronomique, a fondé en 2016 la société **Brigaid**, dans le but d'améliorer la qualité de la nourriture dans les écoles nord-américaines. Il rassemble des chefs professionnels et le personnel des écoles pour remettre en question le système et promouvoir l'éducation alimentaire dans une douzaine d'écoles publiques de New York et de New London, Connecticut.

**Pourquoi l’alimentation scolaire ?**

L’alimentation scolaire est un problème récurent dans de nombreux pays du monde et reflète souvent l’importance qu’ils donnent à leur nourriture. Des pays comme la France ou le Japon sont reconnus internationalement pour faire partie des meilleurs élèves – malgré ce que nous rappellent parfois nos souvenirs – mais d’autres, comme les États-Unis, souffrent d’une qualité de service bien différente.

Plusieurs décennies de politiques favorisant la nourriture industrielle, des budgets de plus en plus réduits, des directives nutritionnelles strictes mais loin d’être cohérentes, ont fait de l’alimentation scolaire américaine l'une des pires.

En 2016 au États-Unis, près de <mark>40% des adultes et 19% des enfants souffrent d'obésité</mark> et ces chiffres ne font qu'augmenter[^1]. Les plus touchés sont les populations les plus pauvres. La plupart des 47 millions d’habitants qui dépendent du gouvernement pour se nourrir vivent dans des déserts alimentaires où aucune alternative n’est proposée et sont forcés de se nourrir dans les fast-food.  
Des millions d’enfants sont contraints de se nourrir dans les cantines scolaires – considérées à la base comme un service mis à disposition de ces populations pour les aider – qui contribuent à entretenir une culture alimentaire déplorable et dangereuse.

### Agir ou réfléchir ?

C'est dans une [conversation de 2018, au MAD Symposium de Copenhague](https://www.youtube.com/watch?v=masuNwQn5Lk), que Dan Giusti donne un signal d'alarme non seulement en ce qui concerne l'alimentation scolaire, mais aussi la santé et le bien-être des générations futures. Il incite l'industrie de la restauration à prendre conscience de la réalité et certains passages mettent en perspective les choix et réflexions présents dans ce milieu :

**~~La nourriture pour "changer le monde"~~**

> La raison pour laquelle le système alimentaire américain est ce qu’il est, c’est que les gens n’en n’ont rien à faire de la bouffe. <mark>Elle passe en second plan par rapport à tout le reste</mark>. Le système alimentaire est tellement cassé que l’idée de le changer à travers la nourriture est naïf. <mark>Avant de vouloir le changer à travers elle, <u>nous devons faire comprendre aux gens pourquoi la nourriture est importante</u></mark>.

> Même dans ce contexte, si nous produisons la meilleure nourriture qui n'a jamais été faite, cela ne veut pas dire que les enfants vont l’apprécier.

**~~Montrer l'exemple en dictant une vision~~**

> <mark>Si tu as 1.24$ pour nourrir des enfants</mark> et que tu commences à prioriser la durabilité, la localité et la saisonnalité, alors tu ne comprends pas comment le monde fonctionne. Tu ne comprends pas le prix des aliments.

> Quand j’ai décidé de faire tout ça, autant que possible pour les bonnes raisons, je ne pense pas que je comprenais vraiment pourquoi je le faisais. \[...\] Je cuisinais pour me sentir bien moi-même. \[...\] En tant que professionnels, nous donnons de l’importance à l’alimentation et la considérons de façon particulière \[...\], mais la plupart des gens ne la voient pas de cette manière. Si vous voulez changer les choses, <mark>nous devons nous mettre à la place des gens et comprendre pourquoi ils mangent certains types d’aliments</mark>. Cela veut peut-être dire que vous allez servir de la nourriture qui ne va pas vous passionner, qui n’est pas assez bonne pour vous, parce qu’elle n’est ni de saison, ni locale ou durable. Même si c’est le cas, vous allez la servir à des gens qui n’ont pas les moyens de l’acheter. <u>Il faut ajuster ses attentes si nous voulons nourrir les gens</u>.

> Les personnes qui mangent dans les restaurants ont pris la décision d’y être. Ils ont fait un choix. <mark>Les enfants qui mangent à la cantine n’ont pas le choix</mark>. Parfois c’est le seul repas qu’ils auront de la journée. \[...\] Quand on nourrit des personnes qui n’ont pas d'argent ou qui n’ont pas de choix, on ressent le besoin de leur dire ce qui est bon et ce qui ne l’est pas. \[...\] Ils devraient être capable de dire “j’aime ou je n’aime pas”, peu importe leur âge. <mark>C’est ce qui va leur permettre d’en apprendre plus sur ce qu’ils mangent</mark>.

**~~Le restaurant gastronomique comme instrument de changement~~**

> Les grands chefs sont aujourd’hui parmi les personnes les plus influentes. <mark>Mais ils ne représentent qu’une infime partie des repas distribués tous les jours dans le monde</mark>. \[...\] Par exemple, les cantines des écoles de New-York servent un million de repas par jour. L'influence de ces chefs peut-elle s’étendre à un monde plus large ?

Dan Giusti met en lumière la déconnexion de l’industrie de la restauration concernant la majorité de la population. Parce que ces problématiques sont avant tout sociales, il nous invite à redéfinir nos actions en ajustant notre vision sur le présent et surtout sur les gens.

**<u>Nous avons tendance à penser les idées avant les individus</u>. <u>Et lorsque nous pensons les individus</u>, <u>nous voulons à tout prix les faire rentrer dans le cadre de nos idées</u>.**

[^1]:[NCHS Data Brief, No. 288, Octobre 2017](https://www.cdc.gov/nchs/data/databriefs/db288.pdf)