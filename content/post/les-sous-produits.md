---
title: Les sous-produits
subtitle: Quel est notre rapport aux sous-produits, comment réduire la quantité de
  matière gâchée et lui donner de la valeur&#8239;?
date: 2020-10-20T00:00:00.000+02:00
tags:
- système alimentaire
- culture alimentaire
- gaspillage alimentaire
- restauration
- zéro déchet
- technique
- recherche
disclaimer: ''
share_img: "/media/les-sous-produits.png"

---
Dans le monde, 58% des émissions de carbone sont créées par le système alimentaire actuel et 40% des aliments produits ne sont pas consommés. La restauration rapide s’avère être le segment le plus pollueur du secteur (en hausse de 5 à 7% par an). Cela amène à un effet boule de neige : augmentation du nombre de repas préparés ainsi que du volume de conditionnements et donc de déchets produits.  

Depuis 2016, la réglementation française en matière de traitement des déchets impose à un producteur générant plus de 10 tonnes de biodéchets par an de procéder à des tris à différents niveaux de sa chaîne de fonctionnement.[^1] Cependant, leur valorisation est limitée. Cela se réduit au compostage et à la méthanisation (récupération du méthane produit par les déchets et retour au sol du digestat), sachant que le méthane est un gaz 23 fois plus polluant que le dioxyde de carbone.
Aucune réglementation n’est mise en œuvre concernant la gestion des emballages ou à la base du problème, la quantité de déchets produits.

### Comment en sommes-nous arrivés là ?

Dans le monde préindustriel, gérer les déchets était simple en plus d'être une évidence. Ils étaient pour la plupart de nature organique. La ville entretenait un cycle dans lequel ses besoins en nourriture étaient alimentés par les déchets qu'elle générait. Aujourd'hui, la majorité de nos déchets sont non-organiques et de natures très diverses, à tel point que leur gestion est devenue une industrie à part entière.

> Quand nous gaspillons la nourriture, nous gaspillons tous les efforts, le travail, l'eau, le soleil, les combustibles fossiles – voire la vie elle-même – qui ont permis de la produire.[^2] <footer>— Carolyn Steel</footer>

Nous vivons dans une société de consommation où tout est jetable et notre tendance à gaspiller illustre parfaitement ce mode de vie. La nourriture est d'ailleurs ce que nous gaspillons le plus et ses moyens de production vont de plus en plus à l'encontre de l'ordre naturel.

### Au sein de la restauration, quelles possibilités avons-nous ?

Le restaurant, vu comme un système en lui-même, fonctionne parfois malgré lui comme un instrument de consommation. La pandémie actuelle nous a montré que personne ne meurt de fin sans restaurants.
L'idée de réduire à grande échelle la production des déchets devient compromise, car il n'y pas de société de consommation sans gaspillage.

Dans ce secteur, notre matière première est malgré tout organique et reste peu – voire non transformée – ce qui facilite la création de nouvelles façons de procéder.

<u>L'un de nos points de bascule consiste en la valorisation des sous-produits</u>.

**Un sous-produit est un <mark>produit obtenu lors de la fabrication d'un produit principal, dérivé d'un autre</mark>.** En cuisine, nous les appelons à tort nos déchets. De la tige d’un brocolis à l'os d'une volaille ou au marc de café, <mark>leur présence est naturelle et constante</mark>.

Avant d'évaluer la quantité de déchets que nous jetons, il est important de déterminer s'ils en sont vraiment.

> Si notre or est fumier, en revanche, notre fumier est or.[^3]
>
> <footer>— Victor Hugo</footer>

Respectivement à Londres et Copenhague, les restaurants Silo et Amass en sont des exemples :

Connu pour sa démarche radicale, Silo est un restaurant "zéro déchet". Ce lieu, mené par Douglas McMaster, ouvre en 2014 et s’affirme comme un précurseur dans son domaine. Le livre issu de ses réflexions <cite>Silo, The Zero Waste Blueprint</cite>, nous apporte de réels outils, techniques et systèmes pour comprendre et agir sur la gestion des déchets. Supprimer les emballages à usage unique, utiliser l'entièreté de la matière organique disponible ou recycler celle non-organique sont quelques unes de ses méthodes.

> Silo est un restaurant qui n'a pas de poubelle : de cette simple limitation pousse un grand arbre. Le produit se trouve à la pointe de l'arbre. Au lieu de se concentrer uniquement sur le produit, nous regardons l'arbre dans son ensemble. En commençant par les racines et en le suivant jusqu'aux feuilles.[^4] <footer>— Douglas McMaster</footer>

Le restaurant Amass fut l’idée de Matthew Orlando, ancien membre du Noma (élu à multiples reprises meilleur restaurant au monde). Sa démarche se base sur l’utilisation des sous-produits comme source principale d'inspiration. Son jardin, situé au sein du restaurant, l’aide à créer une cuisine et un espace résilient, où créativité et innovation sont mots d'ordre.

> Nous commençons avec les sous-produits, <mark>ils sont en quelque sorte recyclés, c’est notre point de départ</mark>. Un beau morceau de poisson ou une belle carotte sont les dernières choses que nous ajoutons dans un plat. \[…\] Nous cuisinons à l’envers, je suppose.[^5]

> <mark>Nous ne les transformons pas avec une idée particulière en tête destinée à un plat. Nous les transformons jusqu'à un état stable, où nous pouvons les stocker.</mark> Nous avons alors cet énorme garde-manger d'ingrédients et de saveurs à utiliser lorsque nous en sommes réellement au processus de création \[…\].[^6] <footer>— Matthew Orlando</footer>

Ces deux restaurants ont en commun l'utilisation d'une technique ancestrale, permettant de réduire drastiquement la matière gaspillée : la fermentation. Grâce à différents micro-organismes, levures, bactéries et enzymes, il est en effet possible de transformer un sous-produit en une substance délicieuse et hautement nutritive.

**Le sous-produit n'est-il donc pas entièrement pris en compte au moment où il n'en devient plus un ?**

Nous avons vu que la notion même de sous-produit en tant que déchet apparait quand le système mis en place au départ est défectueux. Dans la nature sauvage, ces termes n'existent pas. La matière morte nourrit la matière vivante et chaque entité dépend de l'autre.

Il est évident qu'aujourd'hui, en tant qu'individu, nous aurions du mal à réorganiser l'entièreté du système industriel actuel et supprimer son inévitable rejet de déchets. Nous sommes contraints de nous adapter, pas de nous résigner.

**L'avenir nous forcera-t-il à reconsidérer nos déchets à travers de nouveaux usages ?
Devons-nous redéfinir le rôle d'un restaurant afin d'ajuster son fonctionnement ?**

[^1]: https://www.permis-de-exploitation.com/770-l-gestion-dechets.html

[^2]: <cite>Ville Affamée</cite> de Carolyn Steel, 2016, éditions Rue de l’échiquier, p.335

[^3]: <cite>Les Misérables Tome V</cite> de Victor Hugo, 1890, éditions Émile Testard et Cie, p.154

[^4]: “Silo is a restaurant that doesn’t have a bin: from that simple limitation grows a big tree. At the very tip of the tree is the product. Instead of focusing on just the product, we're looking at the whole tree. beginning with the roots and following it all the way to the bright shiny leaves.” <cite>Silo, The Zero Waste Blueprint</cite> de Douglas McMaster, 2019, éditions Leaping Hare Press

[^5]: “We’re starting with the by-products, they’re kind of upcycling, as our starting point of the creative process, and then genuinely, a nice piece of fish or a beautiful carrot is the last thing we had to the dish.  \[…\] We’re kind of cooking backwards I guess.” [ep13 | Responsible Deliciousness with Matt Orlando (11:45) WorldWild Podcast](https://worldwildpod.podbean.com/e/13-responsible-deliciousness-with-matt-orlando/)

[^6]: “We don’t process these into something else, with a particular idea in mind for a dish. We process them to a stable point, where we can store them, and we just have this massive larder of ingredients and flavor to drop on, when we’re actually to the creative process \[…\].” [ep13 | Responsible Deliciousness with Matt Orlando (15:20) WorldWild Podcast](https://worldwildpod.podbean.com/e/13-responsible-deliciousness-with-matt-orlando/)