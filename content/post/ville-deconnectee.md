---
title: Ville déconnectée
subtitle: En tant que citadin, quels rapports avons-nous avec ce/ceux qui nous nourrit
  ?
date: 2020-11-06T00:00:00.000+01:00
tags:
- culture alimentaire
- système alimentaire
- agriculture
disclaimer: ''
share_img: ''
draft: true

---
Seulement 25% de notre nourriture n'a pas été manipulée et transformée par une autre personne.

“nous habitons dans la ville comme s’il s’agissait de la chose la plus naturelle au monde, alors qu’au fond nous vivons toujours sur les terres.” (p.19)

“en 2007, nos dépenses alimentaires représentaient seulement 10% de nos revenus, alors qu’en 1980, elles atteignaient 23%.” (p.16)

“nous laissons la nourriture en second plan pour nous concentrer sur nos vies trépidantes, sans avoir la moindre conscience de ce qui est nécessaire pour alimenter la machine.” (p.16)

“Comme si la chair que nous mettons dans notre bouche n’avait aucun rapport avec l’oiseau vivant. Nous ne faisons tout simplement pas le liens” (p.17)

> La nourriture est le bien le plus dévalorisé qui soit dans notre Occident industrialisé, car <mark>nous avons perdu de vue ce qu’elle signifie. Vivant en ville, nous avons appris à nous comporter comme si nous n’appartenons pas au monde naturel ; comme si nous étions distincts de “l’environnement”</mark>. \[^2\]

"Vous avez de plus en plus de consommateurs qui veulent manger en dehors de chez eux, donc vous avez de plus en plus d'institutions disponibles qui le permettent, où vous pouvez acheter de la nourriture qui a été traitée et transformée par quelqu'un dont vous ne savez absolument rien." \[^3\]

> Le problème réside dans notre mode de vie urbain. Habitant le pays industrialisé le plus ancien du monde (Angleterre, ndlr.), <mark>nous avons perdu le contact avec la vie rurale depuis des siècles</mark>. \[...\] À l’époque préindustrielle, aucun citadin n’aurait pu rester dans une telle ignorance. Avant l’apparition du chemin de fer, l’approvisionnement alimentaire était la principale difficulté que les villes avaient à surmonter. \[...\] Les routes étaient encombrées de charrettes transportant légumes et céréales, les fleuves et les ports regorgeaient de vraquiers et de bateaux de pêche, les rues et les cours étaient envahies par les vaches, les cochons et les poules.\[^1\]

La ville en elle même est conçu pour être déconnectée > dépend des highland, fini par se bouffer elle-même comme Rome, les Sumériens, les Grecs, même s'ils considéraient la campagne bien mieux que nous. (par Sitopia p.190-191)

Cela modifie notre culture : perte des saveurs originale > voir Sitopia

\[^1\]: <cite>Ville Affamée</cite> de Carolyn Steel, 2016, éditions Rue de l’échiquier, p.17-18

\[^2\]: Carolyn Steel, ibid., p.73

\[^?\]: <cite>Sitopia</cite> de Carolyn Steel, 2020, éditions Chatto & Windus, p.17-18

\[^3\]: "You have more and more consumers willing to and wanted to eat food outside of the home, so you have more and more intitutions availble where you can purchase food outside of the home, that's been processed by somebody you don't know anything about" (Mean to be eaten podcast – Amy Trubek on how restaurant culture needs to change, 19:20)