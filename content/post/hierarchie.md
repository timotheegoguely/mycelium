---
title: Hiérarchie
subtitle: Pourquoi repenser la hiérarchie dominante pyramidale&#8239;?
date: 2020-11-06T00:00:00.000+01:00
tags:
- culture
- histoire
- institution
- restauration
disclaimer: ''
share_img: "/media/hierarchie.png"

---
La brigade. Le chef, le sous-chef, le chef de partie, le saucier, le pâtissier, le commis, le chef de rang, le directeur de salle... Autant de postes différents que de responsabilités, d'expertises et d'organisations.  
D'où nous vient cette hiérarchie si stricte qu'elle en devient presque un mythe, admiré voir fantasmé ? Quelles en sont les forces mais surtout les limites ? Pourquoi et comment est-il souhaitable aujourd'hui d'en proposer des alternatives ?

C'est au XIX<sup>e</sup> siècle qu’Auguste Escoffier, témoin du chaos et des conditions de travail catastrophiques présentent dans les cuisines des grands établissements parisiens, décide d’instaurer de nouveaux codes. Finis les vociférations, l’improvisation et les cuisiniers carburant au vin rouge. Place à l’ordre, à l’organisation, à la hiérarchie institutionnalisée.  
L’efficacité de sa méthode fit ses preuves à une époque où les cuisines des palaces parisiens faisaient la part belle aux plats extravagants, pour des clients de plus en plus pressés. D’année en année, grâce à cette nouvelle façon de fonctionner, la cuisine française s'est élevée en vitrine nationale, influant le monde entier. La brigade de cuisine s'est petit à petit instaurée comme la norme dans les cuisines professionnelles et s'est vue enseignée internationalement dans les écoles culinaires.

Aujourd’hui encore, ce système perdure et pendant un siècle, cette influence a conditionné tout un milieu, qui a du mal à s’en défaire. Il ne faut pas nier son efficacité dans certains contextes, mais notre époque n’est plus la même et nos enjeux ont évolué. Il est vrai que certains établissements continuent de servir une “cuisine de roi”, comme était qualifiée celle d’Escoffier, mais tout cela est-il encore nécessaire ?

La hiérarchie pyramidale que l’on constate au sein du restaurant peut en effet devenir limitante. Elle encourage une <mark>volonté superficielle de perfection</mark>. Elle a tendance à <mark>enfermer et uniformiser les connaissances</mark>. Vouloir grimper à tout prix les échelons provoque des comportements toxiques et peut amener à un état d'esprit égoïste, jusqu’à engendrer des blessures psychologiques et sociales. Les individus se retrouvent éloignés du sens premier de leur métier. Ce mal-être se ressent tous les jours au sein d’un grand nombre de cuisines et les témoignages de ces situations sont aujourd'hui courants.

### Comment réinventer ou adapter ce système pour répondre aux problématiques communes actuelles ?

Pour cela, il est intéressant de se pencher sur la façon de gérer une équipe et sur la notion de “leadership”.

**Hiérarchie : classification dans laquelle <mark>les termes classés sont dans une relation de subordination</mark>, chaque terme dépendant du précédent et commandant le suivant.**

**Leadership : le fait d’aider n’importe quel groupe de deux personnes ou plus, à atteindre un but commun.**

Nous pouvons voir part définition qu'amener de nouvelles idées, agir en tant que “leader” n’a rien à voir avec la position d’un individu dans une organisation, à sa place dans la hiérarchie. Si l’ancienneté peut indiquer que quelqu’un possède davantage de compétences, elle n’en est en aucun cas une garantie. Il n’est pas non plus question de charisme ou de génie. La transmission de savoirs peut agir à tous les niveaux. De nombreuses personnes qui n’ont jamais atteint une position hiérarchique de premier plan, qui ne se définissent pas comme “ambitieuses, énergiques, passionnées, décisives” ou qui ne pensent pas être assez compétentes pour gérer les idées, le stress et l'ambiguïté des gens, agissent néanmoins comme des leaders.[^1]

Le leadership ne se base pas sur les grandes décisions. Il se base sur la multitude de petites choses qui rendent les grandes décisions possibles.

Avant toute chose, si l'on souhaite faire avancer un groupe de personnes traitant une problématique ou une action complexe, il est primordial de définir un ou plusieurs objectifs communs. Ces objectifs ne sont pas immuables, ils peuvent changer au fil du temps et de l'avancement de chacun.

La nature même des organisations définit ces objectifs. Si celles-ci suivent un but purement économique, de performance et de répétition, il devient inévitable qu'une hiérarchie oppressante s’y installe.

> Lorsqu'on utilise <mark>un modèle industriel pour organiser le travail</mark>, on peut intégrer quelqu'un aussi vite qu'on le rejette. La personne en elle-même, son intelligence, ses capacités ou son engagement envers la chose faite devient secondaire par rapport à la tache effectuée, à l'action physique.[^2]

Il est malgré tout naïf de vouloir abolir le rôle de “chef”. Nous pensons parfois à tort que certains systèmes, comme l’anarchie, cherchent à supprimer cette position au profit d’une égalité hiérarchique commune. En réalité, nous pouvons nous passer d’une autorité dominante unique ayant un pouvoir psychologique et matériel, pour laisser place à <mark>plusieurs groupes responsables</mark>. Dans ces groupes, la présence d'un chef n'est pas exclue, mais <u><mark>elle n’est pas instituée</mark></u>.  
Le rôle devient fluctuant, peut passer d’une personne à une autre en fonction de l’expertise reconnue de chacun. Quand le “chef” ne respecte pas ses responsabilités, le groupe peut arrêter de le suivre, sans qu’une institution ou qu’un contrat l’en empêche.

> Le travail en restauration est extrêmement collaboratif, il est basé sur le travail d'équipe, mais la culture de la restauration ne l'est pas.[^3]

Ce travail d'équipe commence bien au-delà du restaurant en lui-même, mais dans les médias et dans l'imaginaire collectif, où l’individualité est célébrée, le rôle de chef est omniprésent et majoritairement mis en avant. La plupart des guides, critiques et labels culinaires encouragent et participent à cette imaginaire qui glorifie le résultat final, plus que l'entièreté du processus en amont.

À l’heure actuelle, où nous vivons de grands changements sociétaux, sanitaires et environnementaux, l’objectif des restaurants doit se redéfinir en même temps que la culture qui en gravite. Si la légèreté ou le bonheur d'un repas partagé se suffit parfois à lui-même, il ne doit pas être à tout prix dissocié du monde dans lequel il prend forme. À l’extérieur comme au sein des établissements, il devient important de supporter <u>une parole collective, un engagement et des responsabilités communes</u>. <u>Mettre en avant des groupes plutôt que des individus, en fluidifiant la position de ~~chef~~ responsable</u>, permettrait de s’adapter plus facilement aux enjeux de demain.

**Quels objectifs voulons-nous nous fixer ?
Quelles cultures voulons-nous recréer ou préserver ?
Quels dialogues voulons-nous entretenir au sein de nos communautés ?**

[^1]: traduction personnelle du livre <cite>Do Lead</cite> de Les McKeown, 2014, éditions The Do Book Company

[^2]: traduction personnelle du podcast [<cite>Mean to be eaten</cite> - Amy Trubek on how restaurant culture needs to change](https://www.stitcher.com/podcast/heritage-radio-network/meant-to-be-eaten/e/66685411) (34:00)

[^3]: traduction personnelle du podcast [<cite>Mean to be eaten</cite> - Amy Trubek on how restaurant culture needs to change](https://www.stitcher.com/podcast/heritage-radio-network/meant-to-be-eaten/e/66685411) (35:10)