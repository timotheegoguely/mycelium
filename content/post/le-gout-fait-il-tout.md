---
title: Le goût fait-il tout ?
subtitle: ''
date: 2020-11-27T00:00:00+01:00
tags:
- recherche
- restauration
- culture alimentaire
disclaimer: ''
share_img: ''
draft: true

---
Le goût est la saveur de quelque chose, la caractéristique reconnaissable par le sens gustatif.

Passer un message.

> Nous pouvons avoir une conversation sur à quel point nous sommes responsables, que nous connectons les gens à la nature et comment nous travaillons pour faire de ce monde un meilleur endroit pour vivre, mais rien de cela ne va résonner chez les gens si le plat qu’on leur sert n’est pas délicieux.\[^1\]

Remettre en question les étiquettes et ordres établis. (Empirical Spirit “We are a flavour company”)

On traduit le mot délicieux en japonais par « umami ». L’umami est très simple de par sa fonction biologique. Il vous dit qu’un aliment contient des protéines. C’est une relation directe avec la présence d’un nutriment.

Rosemary Liss\[^3\] : Quelque chose de beau ou intéressant dans le process mais pas bon ou quelque chose de bon mais pas beau. Intérêt artistique, dans la démarche de recherche et d'expérimentation, sans forcément être en quête d'un résultat défini. 

Notre culture et nos habitudes guident ce que l’on mange.

> Même si nous produisons la meilleure nourriture qui n'a jamais été faite, cela ne veut pas dire que les enfants vont l’apprécier. \[^2\]

\[^1\]: [WorldWild Podcast ep13 | Responsible Deliciousness with Matt Orlando](https://open.spotify.com/show/0qraMx9h8o7NdjnVSkjDZj) (29:30)

\[^2\]: [Feeding A Million | Dan Giusti, Founder of Brigaid | MAD Symposium 2018]()

\[^3\]: [Meant To Be Eaten, Rosemary Liss on food as an artistic medium]()