---
title: Pourquoi mycelium ?
subtitle: ''
date: 2020-10-10T00:00:00+02:00
tags:
- recherche
disclaimer: " La version de ce blogue que vous visionnez en ce moment n’est sûrement
  pas celle d’hier et ne sera sans doute pas celle de demain."
share_img: "/media/pourquoi-mycelium.png"

---
Le mycélium est l’appareil végétatif des champignons et de certaines bactéries filamenteuses. Il assure plusieurs grandes fonctions biologiques : exploration, nutrition, croissance, défense… Il crée un réseau qui relie les êtres vivants et la matière morte. Le mycélium décompose la matière pour la transformer en substrat nutritif. Il aide également les arbres et les plantes à étendre la surface d’exploration de leurs racines.

En m’inspirant de ce mécanisme à travers le prisme de la nourriture et de la cuisine, j’essaie de remettre en question le monde qui m’entoure.

La cuisine est pour moi l’acte de s’exprimer à travers le fait de nourrir. Elle agit pour moi comme un médium, une façon d’échanger, de transmettre, de tisser des liens avec autrui et de prendre soin d’eux.  
Sa pratique professionnelle est largement influencée par son environnement. Aujourd’hui, nos paysages sont industriels, notre agriculture est industrielle, nos comportements sont façonnés par l’industrialisme. Ces aspects déterminent notre culture et bien sûr notre cuisine.

**Avec quelles pratiques culinaires, quels systèmes alimentaires pouvons-nous évoluer dans ce monde ?**

**Quels rôles jouent le restaurant dans une société capitaliste et industrielle ?**

**Comment donner de l’importance et comprendre ce/ceux qui nous nourrit ?**

**...**

Écrire sur ces sujets me permet de rassembler et unifier ces réflexions. Les mettre en forme crée un cheminement, un moyen de m’aider à avancer.  
Ce blogue est un espace personnel où je m’exprime librement, en accord avec mes valeurs.