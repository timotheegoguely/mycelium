---
title: Test markdown
subtitle: ''
date: 2015-02-20
tags: []
disclaimer: ''
draft: true

---
You can write regular [markdown](http://markdowntutorial.com/) here and Jekyll will automatically convert it to a nice webpage.  I strongly encourage you to [take 5 minutes to learn how to write in markdown](http://markdowntutorial.com/) - it'll teach you how to transform regular text into bold/italics/headings/tables/etc.\[^1\]

**Here is some bold text**

## Here is a secondary heading

Here's a useless table\[^2\]:

| Number | Next number | Previous number |
| :--- | :--- | :--- |
| Five | Six | Four |
| Ten | Eleven | Nine |
| Seven | Eight | Six |
| Two | Three | One |

How about a yummy crepe?

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg "Légende de l’image")

Here's a code chunk with syntax highlighting:

```javascript
var foo = function(x) {
  return(x + 5);
}
foo(3)
```

### Here is a level 3 heading

> Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula porta felis euismod semper. Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur blandit tempus porttitor. Nullam quis risus eget urna mollis ornare vel eu leo. Etiam porta sem malesuada magna mollis euismod.
> <footer>— Auteur de la citation, <cite>Titre de l’ouvrage</cite></footer>

Here is a list:

* item 1
* item 2
* item 3

\[^1\]: First footnote
\[^2\]: Second footnote