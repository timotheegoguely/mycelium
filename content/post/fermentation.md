---
title: Fermentation
subtitle: ''
date: 2020-11-25T00:00:00+01:00
tags:
- culture alimentaire
- souveraineté alimentaire
- recherche
- technique
disclaimer: ''
share_img: ''
draft: true

---
Fermentation : transformation que subissent certaines matières organiques sous l'action de micro-organismes.

> Considérer cela comme une mode passagère c'est passer à côté de la portée plus large de son éveil culturel : <mark>la fermentation n’est pas l’objet d’une tendance, elle fait l'expérience d'une compréhension.</mark>[^1] <footer>— David Zilber</footer>

> Ces avantages comprennent : <u>une meilleure santé intestinale</u>, <u>une biodisponibilité accrue des nutriments</u>, <u>une réduction du gaspillage alimentaire</u> et même les avantages <u>psychologiques</u> de le traiter comme un animal de compagnie et d'en prendre soin – tout cela avant d'arriver au cœur funky et délicieux de la fermentation : <mark><u>son goût</u></mark>.[^2] <footer>— David Zilber</footer>
  
Le slogan de la fermentation devrait être : <u>Soit guidé par le goût</u>

"Et bien que les microbes soient muets, leur capacité croissante à démocratiser la nourriture et à mettre sa production entre les mains des individus, devrait rendre notre système alimentaire plus résistant et nous donner à tous une voix plus forte."

[^1]:[The Democratisation of Fermentation](https://www.theworlds50best.com/stories/News/the-democratisation-of-fermentation-by-david-zilber.html), David Zilber

[^2]:[The Democratisation of Fermentation](https://www.theworlds50best.com/stories/News/the-democratisation-of-fermentation-by-david-zilber.html), David Zilber