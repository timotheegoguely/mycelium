---
title: Les anti-nutriments
subtitle: Que nous apportent réellement les végétaux que nous consommons&#8239;?
date: 2020-10-26T00:00:00.000+01:00
tags:
- nutrition
- technique
- fermentation
- recherche
disclaimer: Je fais ici l'objet d'observations et d'analyses personnelles. Je ne suis
  pas en mesure d'énoncer des consignes nutritionnelles et celles-ci doivent seulement
  provenir d'un médecin-nutritionniste.
share_img: ''

---
Avant d'aborder le rôle des anti-nutriments, précisons ce que sont les nutriments :

Les nutriments sont l'ensemble des composés organiques et inorganiques nécessaires à l'organisme vivant pour entretenir la vie. Nous pouvons donner comme exemple les lipides, glucides, protides, les minéraux ou les vitamines. Ils sont directement assimilables par l'organisme dans leur état pur.

Chez l'être humain, comme pour beaucoup d'être vivants, c'est à travers la digestion des aliments que nous mangeons, que nous absorbons ces nutriments. C'est l'ensemble de notre système digestif, avec ses nombreuses bactéries et enzymes, qui va transformer les aliments que nous ingérons, pour rendre disponible les nutriments nécessaires à notre bon fonctionnement.  
Mais tous les aliments ne sont pas égaux quant à la biodisponibilité de leurs nutriments, c'est à dire à la quantité et la vitesse à laquelle nous les absorbons.

Certaines substances présentes dans nos aliments entravent même directement cette biodisponibilité en empêchant les nutriments d'être assimilés. **Ce sont les anti-nutriments, qui sont en grande majorité présents dans les aliments d'origine végétale.**

**Quels sont leurs rôles ?**

À l'instar des animaux sauvages, les plantes cherchent à survivre, à perpétuer leur espèce et transmettre leur gènes. Elles créent alors des mécanismes de défense pour éloigner les prédateurs et protéger leurs graines.

Les végétaux fabriquent une grande variété de composés chimiques. C'est là que nous trouvons les anti-nutriments. <mark>Ils sont présents dans les graines de céréales et de légumineuses, dans les racines et les feuilles de certaines plantes. Ces substances diminuent la digestibilité de ces plantes et permettent d'éviter une trop forte pression des prédateurs qui les mangent.</mark>

Cependant, un grand nombre d’espèces animales s'est adapté et a développé différents systèmes pour éliminer ces composés et sont capables de manger ces plantes. Chez les êtres humains qui sont omnivores, ce processus semble plus complexe.

Ainsi, contrairement à certaines opinions, **l’homme n’est pas adapté par nature à consommer certains végétaux en état**. N’oublions pas que les prédateurs principaux des végétaux ne sont pas des humains mais des animaux herbivores ou des insectes phytophages. **Les légumes secs, les céréales et les pseudo-céréales** (quinoa, sarrasin) **ne sont pas directement comestibles dans leur état naturel et n’ont pas été consommés depuis la nuit des temps**.

**Les principaux anti-nutriments :**

* <mark>Les lectines</mark> : **elles ont la capacité de se fixer à des molécules présentes sur nos cellules et peuvent en perturber le fonctionnement**.

* <mark>Les saponines</mark> : **ils ont une action détergente et agissent en dissolvant les membranes des cellules des insectes ou des microbes**. Toutes les plantes contiennent des saponines qui sont le plus souvent concentrés dans les organes de réserve comme les racines et les tubercules.

* <mark>Les inhibiteurs enzymatiques</mark> : ils jouent un rôle important chez les végétaux en empêchant aux prédateurs de digérer entièrement les graines et les noix, en permettant leur conservation. **Les inhibiteurs enzymatiques entravent le travail de nos propres enzymes et gênent l’absorption de nutriments utiles comme les vitamines et les minéraux.**

* <mark>Les phytates</mark> : ces composés transforment les minéraux comme le calcium, le magnésium, le zinc, le fer et le cuivre, ce qui résulte par leur élimination via les urines et de ce fait, bloque la biodisponibilité de ces minéraux indispensables à l’organisme humain. **Seuls les ruminants et les oiseaux sont capables de digérer les phytates en raison de la présence d'enzymes dans leur système digestif qui les dégradent.**

Les aliments qui en contiennent le plus sont les graines de légumineuses comme les lentilles, les fèves, les pois, les haricots et le soja, les céréales et les pseudo-céréales (quinoa, sarrasin) ainsi que les plantes de la famille des solanacées (tomates, aubergines, poivrons, pommes de terre).  
C'est grâce à l'apprentissage des techniques de transformation culinaire que l'homme peut manger ces aliments. **Les légumineuses et les céréales ne doivent pas être consommées en l'état mais doivent être préparées à travers la cuisson, la germination ou la fermentation**, qui peut éliminer plus de 80% des anti-nutriments.  
Ces pratiques sont particulièrement importantes chez les personnes dont la base alimentaire, par choix ou par obligation, est principalement végétarienne.

Dans la majorité des cas et **dans le cadre d’une alimentation diversifiée**, les anti-nutriments ne sont pas présents en quantités suffisantes pour exercer des effets réellement néfastes chez les humains. Tout est une question d'équilibre.

Il est important de retenir que **la composition nutritionnelle d’un aliment ne nous renseigne guère sur la biodisponibilité réelle de ses nutriments**, en particulier pour les aliments d'origine végétale.  
Ainsi, se baser seulement sur la quantité présente de protéines, glucides, lipides, minéraux et vitamines d'un aliment à son état d’origine n'est donc pas un moyen fiable d'établir un régime alimentaire nutritif. <u>La biodisponibilité des nutriments devrait être un critère primordial.</u>

Notre alimentation et nos techniques de transformations, de plus en plus industrielles, ne prennent pas en compte ce critère. Nous nous retrouvons soit avec de la nourriture vide, à haute densité calorique mais à faible valeur nutritionnelle, soit avec des aliments dont on nous vente les vertus, sans considérer nos capacités biologiques à les assimiler et donc, à réellement nous nourrir.

**Par quels moyens, quels aliments et quelles techniques peut-on retrouver une alimentation naturelle et nutritive ?**