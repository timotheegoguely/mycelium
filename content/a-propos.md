---
title: À propos
subtitle: "Je m'appelle Mickaël Goguely. \nJe travaille à Strasbourg, en France. \nAprès
  des études en arts appliqués et design graphique, j’exerce aujourd’hui en tant que
  cuisinier. \nJe questionne et analyse les enjeux du système alimentaire actuel,
  dans le monde de la restauration."
comments: false

---
***

**Contact**

<form name="contact" method="POST" netlify-honeypot="bot-field" netlify>
    <input class="hidden" name="bot-field" />
    <div class="md:flex">
        <p class="flex-1 mt-4 md:mr-4">
            <label for="name">Votre nom <abbr title="Requis">*</abbr></label>
            <input type="text" name="name" id="name" required />  
        </p>
        <p class="flex-1 mt-4 md:ml-4">
            <label for="email">Votre email <abbr title="Requis">*</abbr></label>
            <input type="email" name="email" id="email" required />  
        </p>
    </div>
    <p class="mt-0">
        <label for="message">Votre message <abbr title="Requis">*</abbr></label>
        <textarea name="message" id="message" rows="8" required></textarea>  
    </p>
    <p>
        <input type="submit" value="Envoyer">
    </p>
</form>


***

**Crédits**
Design et développement web : [Timothée Goguely](https://timothee.goguely.com)
Framework & CMS : [Hugo](https://gohugo.io/) + [Forestry](https://forestry.io/)
Typographies : [Degular](https://ohnotype.co/fonts/degular), [Swear](https://www.futurefonts.xyz/ohno/swear) et Courier